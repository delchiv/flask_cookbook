# -*- coding: UTF-8 -*-
import os

from flask import Flask
from flask_mongoengine import MongoEngine


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
INSTANCE_DIR = os.path.abspath(os.path.join(BASE_DIR, '..', 'instance'))
STATIC_DIR = os.path.abspath(os.path.join(BASE_DIR, '..', 'static'))
TEMPLATE_DIR = os.path.abspath(os.path.join(BASE_DIR, '..', 'templates'))

app = Flask(
    __name__,
    instance_path=INSTANCE_DIR,
    instance_relative_config=True,
    static_folder=STATIC_DIR,
    template_folder=TEMPLATE_DIR
)
app.config.from_pyfile("config.py")

db = MongoEngine(app)
