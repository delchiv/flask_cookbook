# -*- coding: UTF-8 -*-
from .app import app

# Blueprints registration
from . import index

app.register_blueprint(index.blueprint)
