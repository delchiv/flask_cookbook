# -*- coding: UTF-8 -*-
from werkzeug import abort

from flask import render_template, request

from . import blueprint
from .models import Product


@blueprint.route('/')
def home():
    return render_template('index.html', products=Product.objects.filter())


@blueprint.route('/product/<key>/')
def product(key):
    product = Product.objects.get(key=key)
    if not product:
        abort(404)
    return render_template('product.html', product=product)
