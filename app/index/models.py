# -*- coding: UTF-8 -*-
import datetime

from ..app import db


class Product(db.Document):
    created_at = db.DateTimeField(default=datetime.datetime.now, required=True)
    key = db.StringField(max_length=255, required=True)
    name = db.StringField(max_length=255, required=True)
    price = db.DecimalField()

    def __str__(self):
        return '<Product %r>' % self.id