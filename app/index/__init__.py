# -*- coding: UTF-8 -*-
from flask import Blueprint

blueprint = Blueprint('index', __name__)


from . import views
from . import filters