# -*- coding: UTF-8 -*-

DEBUG = True
TESTING = True

SECRET_KEY = '[redefine at config_local.py]'

SERVER_NAME = '127.0.0.1:8000'

MONGODB_SETTINGS = {
    'DB': 'my_catalog',
}

try:
    LOCAL_SETTINGS
except NameError:
    try:
        from instance.config_local import *
    except ImportError:
        pass
